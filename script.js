const humidity = document.querySelector("#w-humidity");
const pressure = document.querySelector("#w-pressure");
const windSpeed = document.querySelector("#w-wind");

const changeLocation = document.querySelector("#localModalLabel");
const locationCity = document.querySelector("#w-location");

const country = document.querySelector("#w-country");
const desc = document.querySelector("#w-desc");
const temp = document.querySelector("#w-temp");
const icon = document.querySelector("#w-icon");

const maxTemp = document.querySelector("#w-max-temp");
const minTemp = document.querySelector("#w-min-temp");

const tugma = document.querySelector("#w-change-btn");
const input = document.querySelector("#city");

tugma.addEventListener("click", () => {
  if (input !== "") {
    let name = input.value;
    Weather(name);
  }
});

async function Weather(name) {
  let pagoda = await fetch(
    `https://api.openweathermap.org/data/2.5/weather?q=${name}&appid=2d6a242864886272d5ab939272c6d4b3&units=metric`
  );
  let res = await pagoda.json();
  console.log(res);
  locationCity.textContent = `City: ${res.name}`;
  country.textContent = `Country: ${res.sys.country}`;
  desc.textContent = `Weather Condition: ${res.weather[0].description}`;
  temp.innerHTML = `Temperature: ${res.main.temp}&degC`;
  maxTemp.innerHTML = `Max Temp: ${res.main.temp_max}&degC`;
  minTemp.innerHTML = `Min Temp: -${res.main.temp_min}&degC`;
  icon.src = `http://openweathermap.org/img/w/${res.weather[0].icon}.png`;
  humidity.textContent = `Humidity: ${res.main.humidity}%`;
  pressure.innerHTML = `Air Pressure: ${res.main.pressure}&deg`;
  windSpeed.textContent = `Wind Speed: ${res.wind.speed} km/h`;
}
